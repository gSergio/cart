//
//  CartInteractor.swift
//  Cart
//
//  Created by Alejandro Jiménez Agudo on 13/11/15.
//  Copyright © 2015 Gigigo SL. All rights reserved.
//

import Foundation


class CartInteractor {
	
    private var storage: StorageProtocol
	private let productsInteractor = ProductsInteractor()
	private var products: [String: Product]?
	
	// MARK: - Init

    init(storage: StorageProtocol = Storage.shared) {
        self.storage = storage
    }
    
	// MARK: - Public Methods
	
	func totalAmount() -> Double {
		let cart = self.storage.fetchProducts()		// [(productId, quantity)] in my cart
		let products = self.productDB()				// All Products I can purchase
		
		var amount = 0.0
		
		for (productId, productQuantity) in cart {
			let product = products[productId]
			amount += Double(product!.productPrice)! * Double(productQuantity)
		}
		
		return amount
	}
	
	func totalQuantity() -> Int {
		let cart = self.storage.fetchProducts()
		var total = 0
		
		for productId in cart.keys {
			let quantity = cart[productId]
			total += Int(quantity!)
		}
		
		return total
	}
	
	func addProduct(product: Product) {
		var products = self.storage.fetchProducts()
		var order = self.storage.orderedCart
		
		if order.contains(product.productId) {
			order.removeAtIndex(order.indexOf(product.productId)!)
		}
		order.append(product.productId)
		self.storage.orderedCart = order
		
		var quantity = products[product.productId] ?? 0
		quantity++
		products[product.productId] = quantity
		
		self.storage.save(products)
	}
	
	func productsInCart() -> [Product] {
		let productsPurchased = self.storage.orderedCart
		let productsDict = self.productDB()
		
		var products = [Product]()
		
		for productId in productsPurchased.reverse() {
			let product = productsDict[productId]
			products.append(product!)
		}
		
		return products
	}
	
	func productQuantity(product: Product) -> Int {
		let products = self.storage.fetchProducts()
		
		for (productId, productQuantity) in products {
			if productId == product.productId {
				return productQuantity
			}
		}
		
		return 0
	}
	
	
	// MARK: - Private Helpers
	
	private func productDB() -> [String: Product] {
		guard self.products == nil else {
			return self.products!
		}
		
		let products = self.productsInteractor.productList()
		var productDict = [String: Product]()
		for product in products {
			productDict[product.productId] = product
		}
		
		self.products = productDict
		
		return self.products!
	}

}
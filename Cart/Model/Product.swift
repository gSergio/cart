//
//  Product.swift
//  Cart
//
//  Created by Alejandro Jiménez Agudo on 13/11/15.
//  Copyright © 2015 Gigigo SL. All rights reserved.
//

import Foundation

class Product {
	
	var productId: String!
	var productName: String!
	var productPrice: String!
	
}
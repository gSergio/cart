//
//  ProductsDataManager.swift
//  Cart
//
//  Created by Alejandro Jiménez Agudo on 13/11/15.
//  Copyright © 2015 Gigigo SL. All rights reserved.
//

import Foundation


class ProductsDataManager {
	
	func fetchProducts() -> [Product] {
		var products = [Product]()
		
		let productsPath = NSBundle.mainBundle().pathForResource("products", ofType: "json")
		let productsData = try? NSData(contentsOfFile: productsPath!, options: NSDataReadingOptions.DataReadingMappedIfSafe)
		let productsJson = try? NSJSONSerialization.JSONObjectWithData(productsData!, options: NSJSONReadingOptions.AllowFragments)
		let productArray = productsJson!["products"] as! [[String: String]]
		
		for productDict in productArray {
			let product = Product()
			product.productId = productDict["productId"]
			product.productName = productDict["name"]
			product.productPrice = productDict["price"]
			
			products.append(product)
		}
		
		return products
	}
}
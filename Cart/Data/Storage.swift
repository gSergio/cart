//
//  Storage.swift
//  Cart
//
//  Created by Alejandro Jiménez Agudo on 13/11/15.
//  Copyright © 2015 Gigigo SL. All rights reserved.
//

import Foundation

protocol StorageProtocol {
    var orderedCart: [String] { get set }
    
    func fetchProducts() -> [String: Int]
    func save(products: [String: Int])
}

class Storage: StorageProtocol {
	
	static let shared = Storage()
	
	var orderedCart = [String]()
	
	private var purchasedProducts: [String: Int]?
	
	func fetchProducts() -> [String: Int] {
		if let products = self.purchasedProducts {
			return products
		}
		else {
			self.purchasedProducts = [:]
			return self.purchasedProducts!
		}
	}
	
	func save(products: [String: Int]) {
		self.purchasedProducts = products
	}
	
}

//
//  ProductCell.swift
//  Cart
//
//  Created by Alejandro Jiménez Agudo on 13/11/15.
//  Copyright © 2015 Gigigo SL. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {
	
	@IBOutlet weak var productName: UILabel!
	@IBOutlet weak var productPrice: UILabel!

}

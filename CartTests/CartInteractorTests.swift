//
//  CartInteractorTests.swift
//  Cart
//
//  Created by Sergio López on 4/12/15.
//  Copyright © 2015 Gigigo SL. All rights reserved.
//

import XCTest

@testable import Cart


class CartInteractorTests: XCTestCase {
    
    var cartInteractor: CartInteractor!
    var storageMock: StorageMock!
    
    //MARK: Test Life Cycle
    
    override func setUp() {
        super.setUp()
        
        self.storageMock = StorageMock()
        self.cartInteractor = CartInteractor(storage: self.storageMock)
    }
    
    override func tearDown() {
        self.cartInteractor = nil;
        super.tearDown()
    }
    
    //MARK: Tests
    
    func test_cart_interactor_not_nil() {
        XCTAssertNotNil(self.cartInteractor)
    }
    
    
    func test_one_product_returns_its_price() {
        
        let p1 = Product()
        p1.productId = "1"
        p1.productName = "p1"
        p1.productPrice = "3"
        
        self.storageMock.cart = [p1.productId: 1]
    }
}

//
//  StorageMock.swift
//  Cart
//
//  Created by Sergio López on 4/12/15.
//  Copyright © 2015 Gigigo SL. All rights reserved.
//

import UIKit
@testable import Cart

class StorageMock: StorageProtocol {
    
    var cart: [String: Int]!
    
    //MARK: StorageProtocol
    
    var orderedCart = [String]()
    
    func fetchProducts() -> [String: Int]{
        return self.cart
    }
    
    func save(products: [String: Int])
    {
        
    }
}



class Contact {
    
    // MARK: Properties

    var name: String
    var lastName: String
    
    // MARK: Init
    
    init(_ name: String, lastName: String) {
        self.name       = name;
        self.lastName   = lastName;
    }
}




import UIKit


class ContactListVC: AgendaVC, UITableViewDataSource {
    
    //MARK: - PROPERTIES
    
    var contacts: [Contact] = []
    
    // MARK: - METHODS
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadContacts();
    }
    
    // MARK: PRIVATE
    
    func loadContacts() {
        let contact1 = Contact("Jose", lastName: "Ramirez")
        let contact2 = Contact("Alberto", lastName: "Sanchez")

        contacts = [contact1, contact2]
    }
    
    // MARK: - TableView DataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
}


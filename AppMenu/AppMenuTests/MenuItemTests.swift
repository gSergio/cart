

import XCTest

@testable import AppMenu

class MenuItemTests: XCTestCase {
    
    var menuItem: MenuItem!
    
    override func setUp() {
        super.setUp()
        menuItem = MenuItem(title: "titulo")
    }
    
    func testThatMenuItemHasATitle() {
        XCTAssertEqual(menuItem.title, "titulo")
    }
    
    func testThatMenuItemCanBeAssignedASubTitle() {
        menuItem.subTitle = "Repos contributed to"
        
        XCTAssertEqual(menuItem.subTitle!, "Repos contributed to",
            "Subtitle should be what we assigned")
    }
    
}


//
//  MenuItemsPlistReaderTests.swift
//  AppMenu
//
//  Created by Sergio López on 13/1/16.
//  Copyright © 2016 Sergio López. All rights reserved.
//

import XCTest

@testable import AppMenu

class MenuItemsPlistReaderTests: XCTestCase {

    func testErrorIsReturnedWhenPlistFileDoesNotExist() {
        
        let menuItemReader = MenuItemsPlistReader()
        let menuItemsResult = menuItemReader.readFromFile("noFile")
        
        switch menuItemsResult {
        case .MenuItems:
            XCTAssertTrue(false)
        case .Error:
            XCTAssertTrue(true)
        }
    }

}
